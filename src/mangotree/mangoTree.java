package mangotree;
import java.util.*;
public class mangoTree {
    public static boolean isMangoTree(int rows,int columns,int treeNumber){
        if (treeNumber<columns && treeNumber>=0)
            return true;
        else
            return treeNumber%columns-1==0 || treeNumber%columns==0;
    }
    public static void main(String[] args){
        Scanner sc=new Scanner(System.in);
        int rows=sc.nextInt();
        int columns=sc.nextInt();
        int treeNumber=sc.nextInt();
        if(isMangoTree(rows,columns,treeNumber))
            System.out.println("yes");
        else System.out.println("no");

    }
}
/*
*
* if(tree_num<cols && tree_num>=0)
            return true;
        else{
            return tree_num % cols - 1 == 0 || tree_num % cols == cols;
        }

*
*
*
*
* */
